#include "include/stack_test.h"
#include "include/postfix_test.h"
#include <stdlib.h>

int main()
{

    const struct CMUnitTest tests_for_stack[] = {

        cmocka_unit_test(push_test),
        cmocka_unit_test(pop_test),
        cmocka_unit_test(isFull_test),
        cmocka_unit_test(isEmpty_test),
        cmocka_unit_test(peek_test)

    };

    cmocka_set_message_output(CM_OUTPUT_XML);

    setenv("CMOCKA_XML_FILE", "../log/stack_test_log.xml", 0);

    int8_t rv = cmocka_run_group_tests_name("stack_test", tests_for_stack, setup, teardown);

    const struct CMUnitTest tests_for_postfix[] = {

        cmocka_unit_test(is_operand_test),
        cmocka_unit_test(is_operator_test),
        cmocka_unit_test(infix_to_postfix_test)

    };

    setenv("CMOCKA_XML_FILE", "../log/postfix_test_log.xml", 1);
    int8_t rv1 = cmocka_run_group_tests_name("postfix_test", tests_for_postfix, NULL, NULL);

    return rv && rv1;
}
