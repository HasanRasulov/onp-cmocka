
#include "include/stack_test.h"

int setup(void **state)
{
    Stack *s = createStack(SIZE);
    *state = s;

    return 0;
}
int teardown(void **state)
{
    freeStack((Stack *)*state);
    return 0;
}

void push_test(void **state)
{
    Stack *s = (Stack *)*state;

    push(s, 'a');
    push(s, 'b');

    assert_int_not_equal('a', pop(s));
    assert_int_equal('a', pop(s));
}

void pop_test(void **state)
{
    Stack *s = (Stack *)*state;

    push(s, 'a');
    push(s, 'b');

    assert_int_not_equal('a', pop(s));
    assert_int_equal('a', pop(s));
    assert_int_equal(INT_MIN, pop(s));
}

void isFull_test(void **state)
{
    Stack *s = (Stack *)*state;
    uint8_t i;
    for (i = 0; i < SIZE; i++)
        push(s, 'a' + i);
    assert_true(isFull(s));

    for (i = 0; i < SIZE; i++)
        pop(s);
    assert_false(isFull(s));
}

void isEmpty_test(void **state)
{
    Stack *s = (Stack *)*state;

    assert_true(isEmpty(s));

    push(s, 'z');
    assert_false(isEmpty(s));
}

void peek_test(void **state)
{
    Stack *s = (Stack *)*state;

    push(s, 'a');

    assert_int_equal(peek(s), 'a');
    assert_int_equal(peek(s), 'a');
    pop(s);
    assert_int_equal(peek(s), 'z');
    assert_false(isEmpty(s));
}