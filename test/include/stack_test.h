#ifndef STACK_TEST
#define STACK_TEST

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include "../../src/include/stack.h"

#define SIZE 4

int setup(void **state);
int teardown(void **state);
void push_test(void **state);

void pop_test(void **state);
void isFull_test(void **state);
void isEmpty_test(void **state);
void peek_test(void **state);

#endif // ! STACK_TEST
