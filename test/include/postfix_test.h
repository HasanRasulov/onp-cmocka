#ifndef POSTFIX_TEST
#define POSTFIX_TEST

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>
#include "../../src/include/postfix.h"


void is_operand_test(void** state);
void is_operator_test(void** state); 
void  infix_to_postfix_test(void** state);

bool __wrap_is_operand(char c);


#endif