#include "include/postfix_test.h"



void is_operand_test(void** state){

   assert_true(is_operand('a'));
   assert_true(is_operand('x'));
   assert_false(is_operand('1'));   
   assert_false(is_operand('A'));

}
void is_operator_test(void** state){
  
   assert_true(is_operator('+'));
   assert_true(is_operator('^'));
   assert_false(is_operator('%'));   
   assert_false(is_operator('='));

} 
/*
bool __wrap_is_operand(char c){
    puts("mock");
   return mock_type(bool);

}
*/

void  infix_to_postfix_test(void** state){

    //will_return_always(__wrap_is_operand,true);
    
   
    assert_string_equal(infix_to_postfix("(a+(b*c))"),"abc*+");


}