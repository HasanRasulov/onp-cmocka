#ifndef POSTFIX_H
#define POSTFIX_H

#include "stack.h" 

bool is_operand(char c);

bool is_operator(char c);

char*  infix_to_postfix(const char* in);

#endif
